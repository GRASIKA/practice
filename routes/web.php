<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::middleware(['auth'])->group(function(){
    Route::get('/', function () {
        return redirect('/tasks')->with('success', 'You are successfully logged in'); ;
    });
    
    Route::resource('tasks', 'TaskController');
});

Route::get('/home', 'HomeController@index')->name('home');
